
import com.mycompany.d.coffee.Helper.DatabaseHelper;
import com.mycompany.d.coffee.User;
import com.mycompany.d.coffee.dao.UserDao;

public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        //System.out.println("userDao.getAll()");
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        User user1 = userDao.get(2);
        System.out.println(user1);

        //User newUser = new User("user3","password",2,"F");
        // User insertedUser = userDao.save(newUser);
        //System.out.println(insertedUser);
        //insertedUser.setGender("M");
        /*user1.setGender("F");
        userDao.update(user1);
        User updateUser = userDao.get(user1.getId());
        System.out.println(updateUser);
        
        userDao.delete(user1);
        for(User u: userDao.getAll()){
            System.out.println(u);
        }*/
        for (User u : userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc")) {
            System.out.println(u);
        }

        DatabaseHelper.close();
    }
}
